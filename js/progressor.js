const progress = document.querySelector(".progress-done");

let address = location.href;
let value = 10;

//LIST OF DIFFERENT PAGES

let pages = [
  "https://progressx.netlify.com/pages/page2.html",
  "https://progressx.netlify.com/pages/page3.html",
];

//UPDATES THE PROGRESS BAR BASED ON VALUE
const load = (value) => {
  progress.setAttribute(`data-done`, `${value}`);
  progress.innerHTML = `${value}% Complete`;
  progress.style.width = `${value}%`;
  progress.style.opacity = 1;

  //RESPONSIVE STRING
  window.innerWidth < 800 ? (progress.innerHTML = `${value}%`) : null;
};

//CHANGING VALUE BASED ON URL
pages[0] == address ? load(40) : pages[1] == address ? load(90) : load(10);
